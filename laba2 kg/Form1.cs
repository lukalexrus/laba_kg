﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Drawing.Imaging;


namespace laba2_kg
{
    public partial class Form1 : Form
    {
        bool load=false;
        int cadr = 0;
        double Q1 = 0.02, Q2 = -3 * Math.Pow(10, -6);
        double[] c1 = { 0, 0, (-5) };
        double Ftr = 0.2 * 0.5 * 9.8;
        int frame = 40;

        List<double> fiList = new List<double>() { 0 };
        List<double> vxList = new List<double>() { -9 };
        List<double> vyList = new List<double>() { -1 };
        List<double> aList = new List<double>() { 1 };
        List<List<double>> s = new List<List<double>>() { new List<double>() { 15, 5, 0 } };


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cadr = 0;
            timer1.Interval = 100;
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            //xsdwig += finish;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            paint();
            cadr++;
            glControl1.SwapBuffers();
            if (cadr == frame)
            {
                timer1.Tick -= timer1_Tick;
                timer1.Stop();
            }
            
        }

        List<List<double>> itogo = new List<List<double>>();
        
        public void traek()
        {
            int tmp = cadr;
            cadr = frame;
            {
                itogo = calc(0.03);
            }

            if (checkBox2.Checked)
            {
                GL.Color3(Color.Red);
                GL.Begin(BeginMode.Points);
                for (int i = 0; i < itogo.Count; i++)
                {
                    if (itogo[i][0] > 0 && itogo[i][1] > 0)
                    {
                        GL.Vertex3(itogo[i][0], itogo[i][2], itogo[i][1]);
                    }
                }
                GL.End();
            }
                cadr = tmp;
        }

        int pov = 0;
        public void shPaint()
        {
            Vector3d sh=shar(6,2);
            sh = sh + new Vector3d(0, 0, 1) + new Vector3d(itogo[cadr][0], itogo[cadr][1], itogo[cadr][2]);
            float c = 0,a=0;
            GL.Begin(BeginMode.TriangleFan);
            for (int angle = 0; angle <= 360; angle += 5)
            {
                
                
                for (int ang = 0; ang <= 360; ang += 5)
                {
                    
                    // Координаты x, y повёрнутые на заданный угол относительно начала координат.
                    double x = 1 * Math.Cos(angle * Math.PI / 180) * Math.Cos(ang * Math.PI / 180);
                    double y = 1 * Math.Sin(angle * Math.PI / 180) * Math.Cos(ang * Math.PI / 180);
                    double z = 1 * Math.Sin(ang * Math.PI / 180);
                    Matrix3d tmp = Rz(pov * Math.PI) * new Matrix3d(x, y, z, 0, 0, 0, 0, 0, 0);
                    if (c > 1)
                    {
                        c = 0;
                    }
                    c += 0.01f;
                    GL.Color3(c, 1, c);
                    if (tmp.M12 + sh.X > 0 && tmp.M11 + 1 > 0 && tmp.M13 + sh.Y>0)
                    {
                        GL.Vertex3(tmp.M12 + sh.X, tmp.M11 + 1, tmp.M13 + sh.Y);
                    }
                }
            }
            GL.End();
            pov++;

            
        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            load=true;
            GL.ClearColor(Color.SkyBlue);
            GL.Enable(EnableCap.DepthTest);
            Matrix4 p = Matrix4.CreatePerspectiveFieldOfView((float)(15 * Math.PI / 180), 1, 20, 500);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref p);

            Matrix4 modelview = Matrix4.LookAt(70, 20, 70, 0, 0, 0, 0, 1, 0);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelview);
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if(!load)
            {
                return;
            }
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            

            Bitmap bmp = new Bitmap("D:\\EDU\\studio\\laba2 kg\\laba2 kg\\bin\\Debug\\skybox_front.bmp");
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            int texture = GL.GenTexture();

            GL.GenTextures(1, out texture);
            GL.BindTexture(TextureTarget.Texture2D, texture);


            GL.BindTexture(TextureTarget.Texture2D, texture);

            bmp.UnlockBits(bmpData);
            bmp.Dispose();

            GL.Begin(BeginMode.Quads);

            GL.TexCoord2(0, 0); GL.Vertex2(0, 0);
            GL.TexCoord2(1, 0); GL.Vertex2(256, 0);
            GL.TexCoord2(1, 1); GL.Vertex2(256, 256);
            GL.TexCoord2(0, 1); GL.Vertex2(0, 256);

            GL.End();
            GL.Flush();



            paint();

            glControl1.SwapBuffers();
        }

        public double Func(double r)
        {
            double K=1.0/(4*Math.PI*8.854188*Math.Pow(10,(-12)));
            return Math.Abs((K*Q1*Q2)/Math.Pow(r,2));
        }

        public double length2(double x, double y)
        {
            return Math.Pow(Math.Pow(x,2)+Math.Pow(y,2),0.5);
        }

        public double length3(double x, double y, double z)
        {
            return Math.Pow(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2), 0.5);
        }

        public double fiFunc(double x0, double x1, double y0, double y1)
        {
            double tmp = x0 * y1 - x1 * y0;
            double a = Math.Pow(Math.Pow(x0, 2) + Math.Pow(x1, 2), 0.5) * Math.Pow(Math.Pow(y0, 2) + Math.Pow(y1, 2), 0.5);
            return Math.Acos((double)tmp /a);
        }

    public double Ax(double F, double fi)
        {
            return ((double)(F * Math.Cos(fi))) / 0.2;
        }

    public double Sx(double x0,double v0,double ax,double t)
    {
        return (ax * Math.Pow(t, 2) / 2) + v0 * t + x0;
    }

    public List<List<double>> calc(double t)
        {
            
            for (int i = 0; i <= cadr;i++)
            {
                double x0=s[i][0],y0=s[i][1],vx=vxList[i],vy=vyList[i];
                double tmp = Math.Pow((Math.Pow(length2(x0, y0), 2) + Math.Pow(length3(c1[0],c1[1],c1[2]), 2)),0.5);
                tmp = Func(tmp);//F
                if(Ftr>tmp)
                {
                    tmp = 0;
                }
                else
                {
                    tmp = tmp - Ftr;
                }
                double f1 = fiFunc((-10), 0, x0, y0);
                fiList.Add(f1);
                double ax = Ax(tmp, f1);
                double Sx0 = Sx(x0, vx, ax, t);
                double ay,Sy0,vx1=0,vy1=0;
                if(y0<0)
                {
                    ay = Ax(tmp, f1);
                }
                else
                {
                    ay = -Ax(tmp, f1);
                }
                if(Math.Abs(ay)>0)
                {
                    Sy0 = Sx(y0, vy, ay, t);
                }
                else
                {
                    Sy0 = 0;
                }
                if(Math.Abs(ax)>0)
                {
                    vx1 = vx + ax * t;
                }
                if(Math.Abs(ay)>0)
                {
                    vy1 = vy + ay * t;
                }
                s.Add(new List<double>() { Sx0, Sy0, 0 });
                aList.Add(ax);
                vxList.Add(vx1);
                vyList.Add(vy1);
            }
            return s;
        }

    public Matrix3d Rx(double fi)
    {
        return new Matrix3d(
                1, 0, 0,
             0, Math.Cos(fi), -Math.Sin(fi),
             0, Math.Sin(fi), Math.Cos(fi)); ;
    }

    public Matrix3d Ry(double fi)
    {
        return new Matrix3d(
                Math.Cos(fi), 0, Math.Sin(fi),
             0, 1, 0,
            -Math.Sin(fi), 0, Math.Cos(fi)); ;
    }

    public Matrix3d Rz(double fi)
    {
        return new Matrix3d(
                Math.Cos(fi), -Math.Sin(fi), 0,
             Math.Sin(fi), Math.Cos(fi), 0,
             0,0 , 0); ;
    }
    public Vector3d shar(double t, double tao)
    {
        Matrix3d tmp = Rz(2 * Math.PI * tao) * Ry(Math.PI * t)* new Matrix3d(0, 0, 1,0,0,0,0,0,0);
        return tmp.Row0;
    }


        public void paint()
        {
            int w=15;

            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
           



            //оси
            if(checkBox1.Checked)
            {
                GL.Color3(Color.Black);
                GL.Begin(BeginMode.Lines);
                GL.Vertex3(0, 0, 0);
                GL.Vertex3(20, 0, 0);
                GL.Vertex3(0, 0, 0);
                GL.Vertex3(0, 20, 0);
                GL.Vertex3(0, 0, 0);
                GL.Vertex3(0, 0, 20);
                GL.End();
            }
            //плоскость
            GL.Color3(Color.Salmon);
            GL.Begin(BeginMode.Polygon);
                GL.Vertex3(0,0,0);
                GL.Vertex3(w,0,0);
                GL.Vertex3(w,0,w);
                GL.Vertex3(0, 0, w);
                GL.Vertex3(0,0,0);
             GL.End();

            
             


            GL.PointSize(4);
            stoiki();
            traek();
            shPaint();
        }

        public void stoiki()
        {
            int heigth = -5,weigth=2;
            //стойки
            //1
            GL.Color3(Color.Brown);
            GL.Begin(BeginMode.Polygon);
            GL.Vertex3(-0.01, -0.01, -0.01);
            GL.Vertex3(-0.01, heigth, -0.01);
            GL.Vertex3(-0.01, heigth, weigth);
            GL.Vertex3(-0.01, -0.01, weigth);
            GL.Vertex3(-0.01, -0.01, -0.01);

            GL.Vertex3(weigth, -0.01, -0.01);
            GL.Vertex3(weigth, heigth, -0.01);
            GL.Vertex3(-0.01, heigth, -0.01);


            GL.Vertex3(weigth, heigth, -0.01);
            GL.Vertex3(-0.01, heigth, weigth);
            GL.Vertex3(-0.01, -0.01, weigth);
            GL.Vertex3(weigth, -0.01, -0.01);
            GL.End();

            //2
            GL.Color3(Color.Brown);
            GL.Begin(BeginMode.Polygon);
            GL.Vertex3(15, -0.01, -0.01);
            GL.Vertex3(15, heigth, -0.01);
            GL.Vertex3(15, heigth, weigth);
            GL.Vertex3(15, -0.01, weigth);
            GL.Vertex3(15, -0.01, -0.01);

            GL.Vertex3(15-weigth, -0.01, -0.01);
            GL.Vertex3(15-weigth, heigth, -0.01);
            GL.Vertex3(15, heigth, -0.01);


            GL.Vertex3(15-weigth, heigth, -0.01);
            GL.Vertex3(15, heigth, weigth);
            GL.Vertex3(15, -0.01, weigth);
            GL.Vertex3(15-weigth, -0.01, -0.01);

            GL.End();

            //3
            GL.Color3(Color.Brown);
            GL.Begin(BeginMode.Polygon);
            GL.Vertex3(-0.01, -0.01, 15);
            GL.Vertex3(-0.01, heigth, 15);
            GL.Vertex3(-0.01, heigth, 15-weigth);
            GL.Vertex3(-0.01, -0.01, 15-weigth);
            GL.Vertex3(-0.01, -0.01, 15);

            GL.Vertex3(weigth, -0.01, 15);
            GL.Vertex3(weigth, heigth, 15);
            GL.Vertex3(-0.01, heigth, 15);


            GL.Vertex3(weigth, heigth, 15);
            GL.Vertex3(-0.01, heigth,15- weigth);
            GL.Vertex3(-0.01, -0.01, 15-weigth);
            GL.Vertex3(weigth, -0.01, 15);
            GL.End();


            //4
            GL.Color3(Color.Brown);
            GL.Begin(BeginMode.Polygon);
            GL.Vertex3(15, -0.01, 15);
            GL.Vertex3(15, heigth, 15);
            GL.Vertex3(15, heigth, 15-weigth);
            GL.Vertex3(15, -0.01, 15-weigth);
            GL.Vertex3(15, -0.01,15);

            GL.Vertex3(15 - weigth, -0.01, 15);
            GL.Vertex3(15 - weigth, heigth, 15);
            GL.Vertex3(15, heigth, 15);


            GL.Vertex3(15 - weigth, heigth, 15);
            GL.Vertex3(15, heigth, 15-weigth);
            GL.Vertex3(15, -0.01, 15-weigth);
            GL.Vertex3(15 - weigth, -0.01, 15);

            GL.End();
            String _Name = "skybox_front.bmp";
            //texture = LoadTexture(_Name);
            //GL.Disable(EnableCap.Lighting);

            //GL.Enable(EnableCap.Texture2D);

            //GL.Color4(1, 0, 0, 1);

            //GL.BindTexture(TextureTarget.Texture2D, texture);


        }

        private int texture;

        public int LoadTexture(string file)
        {
            Bitmap bitmap = new Bitmap(file);

            int tex;
            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            GL.GenTextures(1, out tex);
            GL.BindTexture(TextureTarget.Texture2D, tex);

            BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bitmap.UnlockBits(data);


            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);

            return tex;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GL.Rotate(-15, 0, 1, 0);
            glControl1.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GL.Rotate(15, 0, 1, 0);
            glControl1.Invalidate();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            GL.Rotate(-15, 1, 0, 0);
            glControl1.Invalidate();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            GL.Rotate(15, 1, 0, 0);
            glControl1.Invalidate();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            GL.Rotate(15, 0, 0, 1);
            glControl1.Invalidate();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            GL.Rotate(-15, 0, 0, 1);
            glControl1.Invalidate();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            paint();
            glControl1.Invalidate();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            paint();
            glControl1.Invalidate();
        }

       
    }
}
